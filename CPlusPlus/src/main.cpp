#include <iostream>
#include <cmath>

u_int64_t WIDTH = 1920;
u_int64_t HEIGHT = 1080;
u_int32_t MAX_ITERATIONS = 5000;

class Complex {
    public:
        double re = 0;
        double im = 0;
        Complex(double i_im, double i_re){
            re = i_re;
            im = i_im;
        }

        Complex add(Complex operand) {
            double real, imag;
            real = operand.re;
            imag = operand.im;
            
            re += real;
            im += imag;
            Complex newInstance(re, im);

            return newInstance;
        };

        Complex multiply(Complex operand) {
            double real, imag, tmp;

            real = operand.re;
            imag = operand.im;

            tmp = re * real - im * imag;
            im = re * imag + im * real;
            re = tmp;
            Complex newInstance(re, im);

            return newInstance;
        };
};

double get_absolute_of_complex(Complex value) {  
    return sqrt((value.re * value.re) + (value.im * value.im));
}

float get_mandelbrot(u_int64_t max_iterations, Complex value) {
    float n = 0;
    Complex z(0.0, 0.0);
    
    while (get_absolute_of_complex(z) <= 2.0 && n < max_iterations) {    
        z = z.multiply(z).add(value);
        n += 1;
    }

    return n;
}

void start_generating(u_int64_t max_iterations) {
    float RE_START = -2.0;
    float RE_END = 1.0;
    float IM_START = -1.0;
    float IM_END = 1.0;
    u_int32_t* array = new u_int32_t[WIDTH * HEIGHT * 4];

    Complex value(0.0, 0.0);
    float mandelbrot_result = 0;
    u_int8_t pixel_color = 0;

    for(float y=0; y < HEIGHT; y++) {
        for(float x=0; x < WIDTH; x++) {
            Complex value(RE_START + (x / WIDTH) * (RE_END - RE_START), IM_START + (y / HEIGHT) * (IM_END - IM_START));
            mandelbrot_result = get_mandelbrot(max_iterations, value);
            pixel_color = abs(255 - (mandelbrot_result * 255 / max_iterations));
            u_int64_t pixel = x + (WIDTH * y);
            u_int64_t pixel_rgba_index = pixel * 4;
            array[pixel_rgba_index] = pixel_color;
            array[pixel_rgba_index + 1] = pixel_color;
            array[pixel_rgba_index + 2] = pixel_color;
            array[pixel_rgba_index + 3] = 255;
            //std::cout << array[pixel_rgba_index];
        }
    }
    std::cout << array[65000] << std::endl;
    delete[] array;
};

int main() {
    std::cout << "Start..." << std::endl;
    start_generating(MAX_ITERATIONS);
    std::cout << "Done" << std::endl;
}