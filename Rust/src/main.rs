extern crate num;
use num::complex::Complex as complex;

const WIDTH: usize = 1920;
const HEIGHT: usize = 1080;
const MAX_ITERATIONS: u32 = 5000;

fn get_absolute_of_complex(value: complex<f32>) -> f32 {
    ((value.re * value.re) + (value.im * value.im)).sqrt()
}

fn get_mandelbrot(max_iterations: &u32, value: complex<f32>) -> f32 {
    let mut n: f32 = 0_f32;
    let mut z: complex<f32> = complex::new(0.0, 0.0);
    while get_absolute_of_complex(z) <= 2.0 && n < *max_iterations as f32 {
        z = (z * z) + value;
        n += 1_f32;
    }
    n
}

fn start_generating(max_iterations: &u32) {
    const RE_START: f32 = -2.0;
    const RE_END: f32 = 1.0;
    const IM_START: f32 = -1.0;
    const IM_END: f32 = 1.0;
    let mut array: Box<[u8; WIDTH*HEIGHT*4]> = Box::new([0; WIDTH*HEIGHT*4]);

    let mut value = num::complex::Complex::new(0.0, 0.0);
    let mut mandelbrot_result: f32 = 0_f32;
    let mut pixel_color: u8 = 0;

    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            value = complex::new(RE_START + (x as f32 / WIDTH as f32) * (RE_END - RE_START), IM_START + (y as f32 / HEIGHT as f32) * (IM_END - IM_START));
            mandelbrot_result = get_mandelbrot(&max_iterations, value);            
            pixel_color = (255_f32 - (mandelbrot_result * 255_f32 / *max_iterations as f32)) as u8;
            let pixel: usize = x + (WIDTH * y);
            let pixel_rgba_index: usize = pixel*4;

            array[pixel_rgba_index] = pixel_color;
            array[pixel_rgba_index + 1] = pixel_color;
            array[pixel_rgba_index + 2] = pixel_color;
            array[pixel_rgba_index + 3] = 255;
            //print!("{}", array[pixel_rgba_index]);
        }
    };
    println!("{}", array[65000]);
}

fn main() {
    println!("Start...");
    start_generating(&MAX_ITERATIONS);
    println!("Done");
}
