# MandelbrotRustCppTest

A test of Rust and C++ speeds on calculating Mandelbrot fractal

A special thanks to the following guys from Discord's Rust Programming Language community for C++ help.
- `CyanideJunkie`
- `Dew`
- `Nemo157`
- `spectra`
- `#![cfg_attr(goat, no_std)]`

And eveyone else who looked at the code too